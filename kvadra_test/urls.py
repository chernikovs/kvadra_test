from django.conf.urls import include, url
from django.contrib import admin


urlpatterns = [
    url(r'^admin/', include(admin.site.urls)),
    url(r'^api/', include('text_api.urls')),
    url(r'^', include('text_api_demo.urls')),
]
