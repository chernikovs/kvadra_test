Тестовое задание для компании KVADRA
====================================

Установка и запуск:

* в файле kvadra_test/settings.py укажите настройки подключения к БД

* выполните в терминале:

        $ virtualenv . && . bin/activate
        $ pip install -r requirements.txt
        $ python manage.py migrate
        $ python manage.py runserver

* перейдите по адресу http://localhost:8000/
