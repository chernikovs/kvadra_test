from django.conf.urls import url

from . import views


urlpatterns = [
    url(r'^get_text/$', views.get_text, name='get_text'),
    url(r'^upload_text/$', views.upload_text, name='upload_text')
]
