from django.contrib import admin
from .forms import TextForm
from .models import Text


class TextAdmin(admin.ModelAdmin):
    form = TextForm


admin.site.register(Text, TextAdmin)
