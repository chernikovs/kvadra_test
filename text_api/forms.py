from django import forms

from .models import Text


class TextForm(forms.ModelForm):
    class Meta:
        fields = ['text']
        model = Text
