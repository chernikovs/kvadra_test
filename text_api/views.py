import json

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.views.decorators.http import require_POST
from django.http import JsonResponse

from .models import Text


@require_POST
def upload_text(request):
    try:
        body = json.loads(request.body)
        text = body['text']
    except ValueError:
        text = None
    if text:
        new_text = Text(text=text)
        new_text.save()
        return JsonResponse({
            'success': True
        })
    return JsonResponse({
        'success': False
    }, status=400)  # HTTP bad request


def get_text(request):
    text_list = Text.objects.all()
    paginator = Paginator(text_list, per_page=10)
    page = request.GET.get('page')
    try:
        text_items = paginator.page(page)
    except PageNotAnInteger:
        text_items = paginator.page(1)
    except EmptyPage:
        text_items = paginator.page(paginator.num_pages)

    data = [{'text': item.text} for item in text_items]
    return JsonResponse(data={
        'items': data,
        'total_items': paginator.count
    })
