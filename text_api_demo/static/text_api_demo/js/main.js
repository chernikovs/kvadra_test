var app = angular.module('textApp', ['ui.bootstrap'], function($httpProvider) {
    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';
});

app.controller('textController', function($scope, $http) {
    $scope.textItems = [];
    $scope.currentPage = 1;
    $scope.maxSize = 5;

    $scope.loadTextItems = function(page) {
        $http.get('/api/get_text/?page=' + page).then(function(response) {
            var data = response.data;
            $scope.totalItems = data.total_items;
            $scope.textItems = data.items;
        });
    }

    $scope.onPageChanged = function() {
        $scope.loadTextItems($scope.currentPage);
    }

    $scope.createText = function() {
        var text = $scope.newText;
        $http.post('/api/upload_text/', {text: text}).then(function(data) {
            $scope.newText = '';
            $scope.loadTextItems($scope.currentPage);
        });
    }

    $scope.loadTextItems(1);
});
